package com.mycompany.dvdstore.controller;

import com.mycompany.dvdstore.entity.Movie;
import com.mycompany.dvdstore.service.MovieService;

import java.util.Scanner;

public class MovieController {

    private MovieService movieService = new MovieService();

    public void addUsingConsole() {
        // User Inputs
        System.out.println( "Enter movie name : " );
        Scanner nameScanner = new Scanner(System.in);
        String movieName = nameScanner.nextLine();

        System.out.println( "Enter movie genre : " );
        Scanner genreScanner = new Scanner(System.in);
        String movieGenre = genreScanner.nextLine();

        // Process
        Movie newMovie = new Movie();
        newMovie.setTitle(movieName);
        newMovie.setGenre(movieGenre);

        MovieService movieService = new MovieService();
        movieService.registerMovie(newMovie);

        System.out.println("Process ended.");
    }
}
